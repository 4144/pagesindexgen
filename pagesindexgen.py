#! /usr/bin/env python
# -*- coding: utf8 -*-
#
#  PagesIndexGen
#  Copyright (C) 2016  Andrei Karas (4144)
#
#  This file is part of The ManaPlus Client.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import re
import sys

filt = re.compile(".+", re.IGNORECASE)
indexName = "index.html"

def showHelp():
    print("PagesIndexGen v0.1")
    print("Usage:")
    print("  ./pagesindexgen.py filedir sitedir template")
    print("Example:")
    print("  ./pagesindexgen.py")
    print("  ./pagesindexgen.py .")
    print("  ./pagesindexgen.py . /")
    print("  ./pagesindexgen.py . / default")

def readTemplate(name):
    with open(template + os.path.sep + name, "r") as f:
        return f.read()

def saveTemplate(name, data):
    with open(name + os.path.sep + indexName, "w") as w:
        w.write(data)

def getFullRelativePath(relativeDir, fileName):
    if len(relativeDir) > 0:
        relativePath = relativeDir + os.path.sep + fileName
    else:
        relativePath = fileName
    return relativePath

def buildIndex(curDir, relativeDir, dirName, dirs, files, backLink):
    indexTemplate = readTemplate("index.tpl")
    dirTemplate = readTemplate("dir.tpl")
    fileTemplate = readTemplate("file.tpl")

    data = ""

    if backLink == True:
        backTemplate = readTemplate("back.tpl")
        data = data + backTemplate.format(
            name = "..",
            url = ".." + os.path.sep + indexName,
            sitedir = dirName,
            urldir = relativeDir,
            filedir = curDir,
        )

    for path in dirs:
        data = data + dirTemplate.format(
            name = path,
            url = path + os.path.sep + indexName,
            sitedir = dirName,
            urldir = relativeDir,
            filedir = curDir,
        )

    for name in files:
        fullPath = os.path.abspath(curDir + os.path.sep + name)
        relativePath = getFullRelativePath(relativeDir, name)
        data = data + fileTemplate.format(
            name = name,
            url = name,
            sitedir = dirName,
            urldir = relativeDir,
            filedir = curDir,
        )

    saveTemplate(curDir, indexTemplate.format(
        sitedir = dirName,
        url = relativeDir,
        filedir = curDir,
        body = data
    ))

def processDir(curDir, relativeDir, dirName, backLink):
    curFiles = os.listdir(curDir)
    dirs = []
    files = []
    for fileName in curFiles:
        if fileName[0] == "." or fileName == indexName:
            continue
        fullPath = os.path.abspath(curDir + os.path.sep + fileName)
        relativePath = getFullRelativePath(relativeDir, fileName)
        if not os.path.isfile(fullPath):
            dirs.append(fileName)
        elif filt.search(fileName):
            files.append(fileName)

    dirs.sort()
    files.sort()

    buildIndex(curDir, relativeDir, dirName, dirs, files, backLink)

    for path in dirs:
        fullPath = os.path.abspath(curDir + os.path.sep + path)
        relativePath = getFullRelativePath(relativeDir, path)
        processDir(fullPath, relativePath, path, True)

num = len(sys.argv)
if num < 1 or num > 4:
    showHelp()
    exit(1)
if num > 1:
    filePath = sys.argv[1]
else:
    filePath = "."
if num > 2:
    sitePath = sys.argv[2]
else:
    sitePath = ""
if num > 3:
    template = sys.argv[3]
else:
    template = "default"
template = "templates" + os.path.sep + template

processDir(filePath, sitePath, "", False)
